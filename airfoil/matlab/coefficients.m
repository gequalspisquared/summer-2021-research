function [] = coefficients()

    % Written by Nicholas Crane
    %
    % Computes the cl, cd, and cp of an airfoil for result 
    % validation
    
    % clearscreen
    clc;
    
    % pressure & xy files
    pressure_files = ["p_6_100.txt", "p_9_120.txt"];
    xy_files       = ["xy_6_100.txt", "xy_9_120.txt"];
    actual_cp      = ["cp_actual_6.txt", "cp_actual_9.txt"];
    
    % simulation parameters
    p_inf   = 0.7142825; % 
    rho_inf = 1.0;       % 
    V_inf   = 0.3;       % 
    alpha   = 6.0;       % rad
    
    q_inf = 0.5*rho_inf*V_inf*V_inf;
    
    % for each case
    for i = 1 : length(pressure_files)
        % load data
        pressure = importdata(pressure_files(i));
        XY       = importdata(xy_files(i));

        % rotate xy values so that chord lies on x-axis
        Rz = [cosd(alpha), -sind(alpha); sind(alpha), cosd(alpha)];
        for j = 1 : length(XY)
            XY(j,:) = Rz*XY(j,:)';
        end
        X = XY(:,1);
        Y = XY(:,2);

        % compute and plot Cp from pressure
        Cp = pressureToCp(pressure, p_inf, q_inf);
        actual_data = importdata(actual_cp(i));
        figure;
        hold on;
        plot(X, Cp,'k','LineWidth',2);
        plot(actual_data(:,1),actual_data(:,2),'r-.','LineWidth',2);
        hold off;
        grid on;
        xlim([-0.05,1.05]);
        xlabel("x/L_c");
        ylabel("Cp");
        title ("Cp vs x/L_c");
        legend("Current Data", "Yeh {\it et al.}");
        set(gcf,'position',[100,100,650,500])

        % compute Cn and Ca from Cp
        X_full = [X; X(1)];
        Y_full = [Y; Y(1)];
        Cn = 0;
        Ca = 0;
        for j = 1 : length(X)
            Cn = Cn + Cp(j)*(X_full(j) - X_full(j+1));
            Ca = Ca + Cp(j)*(Y_full(j+1) - Y_full(j));
        end

        % compute Cl and Cd from Cn and Ca
        Cl = Cn*cosd(alpha) - Ca*sind(alpha);
        Cd = Cn*sind(alpha) + Ca*cosd(alpha);
        Cl = -Cl; % flip sign because I traversed the wing
        Cd = -Cd; % in the opposite direction

        fprintf("Cl = %f\n", Cl);
        fprintf("Cd = %f\n", Cd);
    end
    
end


% ----- %


function [Cp] = pressureToCp(p, p_inf, q_inf)

    % Written by Nicholas Crane
    %
    % Computes Cp from freestream constants and pressure around
    % airfoil
    
    Cp = (p - p_inf) ./ q_inf;
    return;
    
end

