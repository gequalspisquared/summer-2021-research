function [] = coefficients_forces()

    % Written by Nicholas Crane
    %
    % Reads the forces.dat file from charles to determine the 
    % AVERAGE lift and drag coefficients
    
    % clearscreen
    clc;
    
    % data-file location
    forces_file_location = ["../charles/6_100/airfoil_forces.dat", ...
                            "../charles/9_100/airfoil_forces.dat"];
    
    % simulation parameters
    rho_inf = 1.0;
    U_inf   = 0.3;
    z_len   = 0.1; % total width of simulation
    
    q_inf = 0.5*rho_inf*U_inf*U_inf;
    
    % for each test case 
    for i = 1 : length(forces_file_location)
        % import data
        data_raw = importdata(forces_file_location(i), ' ', 3);
        data = data_raw.data;

        % forces
        fx = mean(data(2:end,10)); % ignore the first data point as the 
        fy = mean(data(2:end,11)); % solution likely hasn't converged

        % coefficients of drag and lift
        cl = fy/q_inf/z_len;
        cd = fx/q_inf/z_len;

        % print
        fprintf("Cl = %f\n", cl);
        fprintf("Cd = %f\n", cd);
    end
    
end

