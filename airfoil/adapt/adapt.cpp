#include "UgpSmoothAdapt.hpp"

class MyUgpSmoothAdapt : public UgpSmoothAdapt {

public:

  MyUgpSmoothAdapt() {

    if (mpi_rank == 0)
      cout << "MyUgpSmoothAdapt()" << endl;
    
  }

  /*
  bool setTargetLengthScaleHook(double * cv_delta) {
    
    // if you can identify geometrically or through the solution
    // what you want the grid length scale to be, you can set it
    // in this hook routine, and return true. At present, you can
    // only identify a local isotropic length scale.

    // Note that not all the regular structures are available in this
    // routine because the grid is in a state of iterative adaptation,
    // and all elements have a structured description. The exception to
    // this is the unstructured noocv_i/v which is built temporarily for
    // WINDOW adaptation and for this hiook routine.
    
    // Example: set the length scale inside the sphere at 0,0,0, radius 0.5
    // to 0.01. Here we make use of the fact that we do have the unstructured 
    // noocv_i/v...
    
    FOR_INO {
      no_flag[ino] = 0;
      double r = sqrt(DOT_PRODUCT(x_no[ino],x_no[ino]));
      if (r < 0.5)
	no_flag[ino] = 1;
    }
    updateNoData(no_flag,MAX_DATA);
    FOR_ICV {
      bool all_flagged = true;
      for (int noc = noocv_i[icv]; noc != noocv_i[icv+1]; ++noc) {
	const int ino = noocv_v[noc];
	if (no_flag[ino] == 0) {
	  all_flagged = false;
	  break;
	}
      }
      if (all_flagged) {
	// all our nodes were inside the sphere, so refine...
	cv_delta[icv] = 0.01;
      }
    }
    
    return(true);
  
  }
  */

};


int main(int argc,char * argv[]) {

  try {
    
    CTI_Init(argc,argv,"adapt.in");
    
    {
      
      MyUgpSmoothAdapt solver;
      
      solver.init();
      
      solver.run();

    }

    CTI_Finalize();
    
  }
  catch (int e) {
    if (e == 0)
      CTI_Finalize();
    else
      CTI_Abort();
  }
  catch(...) {
    CTI_Abort();
  }
    
  return(0);

}

