#include "UgpPre.hpp"

class MyPre : public UgpPre {

public:

  MyPre() {

    // register your own data here if you want, or you can
    // do it in the input file using REGISTER_* params.
    
  }

  /*

  // you can transform the mesh manually using the 
  // following hook...

  void transformMeshHook() {

    const double beta = 5.4;
    const double Ly = 4.0;
    const int Ny = 100;
    const double dy = Ly / (double)Ny;
    
    FOR_INO {
      const double yp = x_no[ino][1]/Ly;
      x_no[ino][1] = Ly*sinh(beta*yp)/sinh(beta);;
    }
    
  }

  // a finalHook() is available. The following is an example
  // of splitting one zone into several based on face geometry. 
  
  void finalHook() {

    int nozzle_inside  = addFaZone("NOZZLE_INSIDE",FA_ZONE_BOUNDARY);
    int nozzle_lip     = addFaZone("NOZZLE_LIP",FA_ZONE_BOUNDARY);
    int nozzle_outside = addFaZone("NOZZLE_OUTSIDE",FA_ZONE_BOUNDARY);
   
    int wall = getFaZoneIndex("wall");
    assert(wall >= 0);

    FOR_IFA {
      if (fa_zone[ifa] == wall) {
	// decide if this face should stay in wall, or be part of another zone...
	// note - the face normal is teh outward normal wrt the fluid!...
	double x_fa[3],n_fa[3];
	calcFaceCenterAndNormal(x_fa,n_fa,ifa);
	if ( sqrt(x_fa[1]*x_fa[1] + x_fa[2]*x_fa[2]) < 1.05 ) {
	  // this is on the nozzle. 
	  if ( -n_fa[0] > 0.9*sqrt(DOT_PRODUCT(n_fa,n_fa)) ) {
	    fa_zone[ifa] = nozzle_lip;
	  }
	  else if ( n_fa[1]*x_fa[1] + n_fa[2]*x_fa[2] < 0.0 ) {
	    fa_zone[ifa] = nozzle_outside;
	  }
	  else {
	    fa_zone[ifa] = nozzle_inside;
	  }
	}
      }
    }

    
  }
  */

};
  
int main(int argc,char * argv[]) {
  
  try {
    
    CTI_Init(argc,argv,"prepro.in");

    {
      
      MyPre solver;
      
      if (checkParam("HELP")||checkParam("help")) {
	solver.help();
	throw(0);
      }
      
      solver.doit();
      
    }

    CTI_Finalize();
    
  }
  catch (int e) {
    if (e == 0)
      CTI_Finalize();
    else
      CTI_Abort();
  }
  catch(...) {
    CTI_Abort();
  }
  
  return(0);
  
}
