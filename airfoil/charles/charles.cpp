#include "UgpCvCfIdealGas.hpp"

// ========================================================
//
// Compressible LES solver: charles
//
// Solves the compressible Navier-Stokes equations based
// on constant-gamma ideal gas law.
//
// ========================================================

// an example of how to customize charles. Note that you
// also have to instantiate this executable in the main at the
// bottom of this file.

class MyCharles : public UgpCvCfIdealGas
{

private:
    //double gamma,rho_ref,p_ref;
    //double * rho_error;

public:
    MyCharles()
    {

        if (mpi_rank == 0)
            cout << "MyCharles()" << endl;

        // get input parameters...
        //gamma   = getDoubleParam("GAMMA",1.4);
        //rho_ref = getDoubleParam("RHO_REF");
        //p_ref   = getDoubleParam("P_REF");

        // register data...
        //rho_error = NULL; registerData(rho_error, "RHO_ERROR", CV_DATA);
    }

    /*
  // set an initial condition...
  void initialHook() {
    
    if (step == 0) {

      // the initial condition should only be applied when the 
      // step is zero...
      
      if (mpi_rank == 0)
	cout << "MyCharles::initialHook()" << endl;

      // for an initial condition, set the conserved variables...
      double u_init[3] = { 1.0, 0.0, 0.0 };
      FOR_ICV {
	rho[icv] = rho_ref;
	FOR_I3 rhou[icv][i] = rho_ref*u_init[i];
	rhoE[icv] = p_ref/(gamma-1.0) + rho_ref*DOT_PRODUCT(u_init,u_init);
      }
      
    }

  }

  // do something after every time step...
  void temporalHook() {
    
    if (step%check_interval == 0) {

      if (mpi_rank == 0)
	cout << "MyCharles::temporalHok()" << endl;

    }

  }
  */
};

// ======================================================
// main
// ======================================================

int main(int argc, char *argv[])
{

    try
    {

        // initialize the environment: basically
        // initializes MPI and parameters...

        CTI_Init(argc, argv, "charles.in");

        // the solver...

        {

            MyCharles solver;

            solver.init();

            solver.run();
        }

        // finalize the environment: reports parameter usage, shuts down MPI...

        CTI_Finalize();

        // error management...
    }
    catch (int e)
    {
        if (e == 0)
            CTI_Finalize();
        else
            CTI_Abort();
    }
    catch (...)
    {
        CTI_Abort();
    }

    return (0);
}
