clc, clear all%, close all

%% Operator L
% load data
List = dir('Output_L_*.txt') ;
Data = cell(length(List), 5) ;

TotalNNZ = 0 ;
for FileIDX = 1:1:length(List)
    FileName = List(FileIDX).name ;
    disp(['Loading file: ' FileName '....'])
    MPI_RANK = cell2mat(textscan(FileName, 'Output_L_%u.txt')) ;
    FormSpec = sprintf('MPI_RANK %u, 5*NCV = ', MPI_RANK) ;
    FormSpec = [FormSpec '%u'] ;
    
    %% Load NCV
    fileID = fopen(FileName, 'r');
    dataArray = textscan(fileID, FormSpec, 1);
    NCV5 = cell2mat(dataArray) ;
    fclose(fileID);
    Data{FileIDX, 1} = MPI_RANK ;
    Data{FileIDX, 2} = NCV5/5 ;
    
    %% Load L
    fileID = fopen(FileName, 'r');
    dataArray = textscan(fileID, '%f%f%f%u%u%*s%*s', 'Delimiter', '\t', 'HeaderLines', 2);
    ThisOptL  = cell2mat(dataArray(1:3)) ;
    ThisIndx  = cell2mat(dataArray(4:5)) ;
    
    fclose(fileID);
    Data{FileIDX, 3} = ThisOptL ;
    Data{FileIDX, 4} = ThisIndx ;
    
    %% Load x_cv and baseflow
    fileID    = fopen(sprintf('Coor_L_%u.txt', MPI_RANK), 'r');
    dataArray = textscan(fileID, '%*s%f%f%f%f%f%f%f%*s', NCV5/5, 'Delimiter', '\t', 'HeaderLines', 1);
    %                              id x y l r u v w  e
    fclose(fileID);
    Data{FileIDX, 5} = cell2mat(dataArray) ;
    
    %% calc # of non-zero elements
    TotalNNZ = TotalNNZ + size(ThisIndx, 1) ;
end

TotalNCV = sum(cell2mat(Data(:, 2))) ;

for MPI_IDX = 1:1:size(Data, 1)
    if MPI_IDX == 1
        ShiftIDX = 0 ;
    else
        ShiftIDX = sum(cell2mat(Data(1:MPI_IDX-1, 2))) ;
    end
    
    IdxR  = double(Data{MPI_IDX, 4}(:, 1)) ;
    IdxC  = double(Data{MPI_IDX, 4}(:, 2)) ;
    L1    = Data{MPI_IDX, 3}(:, 1) ; 
    L2    = 1i*Data{MPI_IDX, 3}(:, 2) + Data{MPI_IDX, 3}(:, 3) ;
    ThisNCV = double(Data{MPI_IDX, 2}) ;
    
    Idx1  = IdxR ;
    for i = 1:1:length(IdxR)
        VarShift = floor(IdxR(i)/ThisNCV) ;
        Idx1(i)  = TotalNCV*VarShift + (IdxR(i) - VarShift*ThisNCV) + ShiftIDX + 1;
    end
    
    Idx2  = IdxC ;
    for i = 1:1:length(IdxC)
        VarShift = floor(IdxC(i)/ThisNCV) ;
        Idx2(i)  = TotalNCV*VarShift + (IdxC(i) - VarShift*ThisNCV) + ShiftIDX + 1;
    end    
    
    Data{MPI_IDX, 3} = [L1, L2] ;
    Data{MPI_IDX, 4} = [Idx1, Idx2] ; 
end
clear Idx1 Idx2 IdxC IdxR L1 L2 List dataArray ThisIndx ThisOptL

OptL = cell2mat(Data(:, 3)) ;
Idx  = cell2mat(Data(:, 4)) ;
Base = cell2mat(Data(:, 5)) ;
L1   = sparse(Idx(:, 1), Idx(:, 2), OptL(:, 1)) ;
L2   = sparse(Idx(:, 1), Idx(:, 2), OptL(:, 2)) ;

% assignin('base', ['OptL_' dig2str(size(Data, 1), 2)], L1)
% assignin('base', ['OptL_Beta_' dig2str(size(Data, 1), 2)], L2)
% assignin('base', ['Base_' dig2str(size(Data, 1), 2)], Base)

assignin('base', 'OptL', L1)
assignin('base', 'OptL_Beta', L2)
assignin('base', 'Base', Base)


save('MatrixL_lambda2PI.mat', '*OptL*', '*Base*')
