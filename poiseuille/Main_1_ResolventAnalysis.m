clc, clear all,

NEv         = 128 ;
opts.tol    = 1E-10 ;
opts.p      = 2*128 ;
% opts.maxit  = 0 ;

%% ====================================
load('MatrixL_lambda2PI.mat')
% Base, OptL, OptL_Beta
%% ====================================

M  = size(OptL, 1)/5 ;
N  = size(OptL, 2)/5 ;
FO = spdiags([Base(:, 3); Base(:, 3); Base(:, 3); Base(:, 3); Base(:, 3)].^(+0.5), 0, M*5, N*5) ;
FI = spdiags([Base(:, 3); Base(:, 3); Base(:, 3); Base(:, 3); Base(:, 3)].^(-0.5), 0, M*5, N*5) ;
iI = sparse(1:1:M*5, 1:1:N*5, 1i) ;

%% Vary beta
beta =  1;
    
    OptL_new =      real(OptL_Beta)/1.0^2*beta^2 + OptL ...
              +1i * imag(OptL_Beta)/1.0*beta;

%% Bi-global

M = size(OptL_new, 1) ;
N = size(OptL_new, 2) ;

tic
[VEig, DE] = eigs(1i*OptL_new, NEv, 'sm', opts) ;
toc
BiG = cell(2, 2) ;
BiG{1, 1} = diag(DE) ;
BiG{1, 2} = VEig ;
          
 EVal1 = BiG{1, 1} ;
 EVec1 = BiG{1, 2} ;
 [Y1, I1] = sort(Base(:, 1)) ;
 I1 = I1 + length(Y1) ;

figure;
hold on;
plot(real(EVal1)*100, imag(EVal1)*100, 'r+'); % the factor of 100 is due
                                              % the fact that the mag of 
                                              % the eigenvalues depends
                                              % on the speed
select_script_fixed();
hold off;
ylim([-1,0.1]);
xlim([0,1.1]);
title ("Spectrum (Resolvent Analysis)", 'FontSize', 20);
xlabel("\omega_r", 'FontSize', 24);
ylabel("\omega_i", 'FontSize', 24);
set(gcf,'position',[1000,1000,720,560]);
legend("CharLin", "Schmid", 'FontSize', 16, 'Location', 'southwest');

figure;
hold on;
data = importdata("EigenValue.txt");
plot(data(:,1)*100, data(:,2)*100, 'r+'); % the factor of 100 is due
                                          % the fact that the mag of 
                                          % the eigenvalues depends
                                          % on the speed
select_script_fixed();
hold off;
ylim([-1,0.1]);
xlim([0,1.1]);
title ("Spectrum (BiGlobal)", 'FontSize', 20);
xlabel("\omega_r", 'FontSize', 24);
ylabel("\omega_i", 'FontSize', 24);
set(gcf,'position',[1000,1000,720,560]);
legend("CharLin", "Schmid", 'FontSize', 16, 'Location', 'southwest');
% figure
% plot(abs(EVec1(I1, 1))/max(abs(EVec1(I1, 1))), Y1, 'r.-'), hold on


%     %% Resolvent
%     OmegaVec = [0.03,0.06,0.09,0.128,0.158,0.18]*2*pi*0.6; %linspace(0.0,0.3,41)*2*pi*0.6;
%     Resl = cell(length(OmegaVec), 7) ;
%     for i = 1:1:length(OmegaVec)
%         Omega  = OmegaVec(i) ;
%         Resl{i, 1} = Omega ;
%         disp(['Running Beta = ' num2str(beta)]);
%         disp(['Running Omega = ' num2str(Omega)]);
%  
%         R1SVD  = FO *(-iI *Omega - OptL_new)*FI ;
%         
%         tic
%         R1EIG  = R1SVD * R1SVD' ;
%         [V2, D] = eigs(R1EIG, NEv, 'sm', opts) ;
%         Resl{i, 2} = diag(D) ;
%         Resl{i, 3} = V2 ; % forcing mode
%         R1EIG  = R1SVD' * R1SVD ;
%         [V2, D] = eigs(R1EIG, NEv, 'sm', opts) ;
%         Resl{i, 4} = V2 ; % response mode
%         
%         toc
%         
%         %     tic
%         %     [U, S, V1] = svds(R1SVD, NEv, 0) ;
%         %     Resl{i, 5} = diag(S) ;
%         %     Resl{i, 6} = U ;   % forcing
%         %     Resl{i, 7} = V1 ;  % response
%         %     toc
%     end
%     
%     %% ====================================
%      assignin('base', ['Resl_beta' num2str(beta)], Resl)
%      save(['MatrixL_beta' num2str(beta) '_Resl_03.mat'],['Resl_beta' num2str(beta)],'-v7.3')
%     %assignin('base', 'Resl_beta3PI', Resl)
%     %save(['MatrixL_beta3PI_Resl_01.mat'],'Resl_beta3PI','-v7.3')
%     %% ====================================
%     clear *Resl_beta*


