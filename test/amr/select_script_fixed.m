function output = select_script_fixed(run)

% This file contains Matlab codes for the stability and sensitivity
% analysis of channel flows, Poiseuille and Couette flows. These
% are parts of the tutorial: "Analysis of fluid systems: stability,
% receptivity, sensitivity" by Peter Schmid and Luca Brandt,
% published in Applied Mechanics Reviews, 66(2), 2014.
%
% The main programs are
%
% TransientGrowth.m    : compute the transient growth curve G(t)
% OptimalDisturbance.m : compute the the optimal initial condition 
%                          and the corresponding flow response
% Neutral_a_Re.m       : compute the maximum optimal growth and the
%                          least stable eigenvalue in the Reynolds-
%                          alpha plane
% Neutral_alpha_beta.m : compute the maximum optimal growth and the
%                          least stable eigenvalue in the alpha-
%                          beta plane
% Resolvent.m          : compute the resolvent norm for real and
%                          complex frequency omega
% NumRange.m           : compute the spectrum and numerical range
%                          of the stability operator
%
% To execute, replace the argument 'run' by the string obtained
% from the corresponding function name, sans the suffix.

if (nargin < 1)
    run = 'TransientGrowth';
end

switch run
  case 'TransientGrowth'
    output = TransientGrowth();
  case 'OptimalDisturbance'
    output = OptimalDisturbance();
  case 'Neutral_a_Re'
    output = Neutral_a_Re();
  case 'Neutral_alpha_beta'
    output = Neutral_alpha_beta();
  case 'Resolvent'
    output = Resolvent();
  case 'NumRange'
    output = NumRange();
  otherwise
    warning('AMR:nodemo', 'No such demo');
end

end

function output = TransientGrowth()

% compute the Orr-Sommerfeld matrix for three-
% dimensional Poiseuille or Couette flows,
% compute the energy weight matrix and determines
% transient growth curve G(t)
%
% INPUT
%
% Re        = Reynolds number
% alpha     = alpha (streamwise wave number)
% beta      = beta  (spanwise wave number)
% iflow     = type of flow  (Poiseuille=1, Couette=2)
% N         = total number of modes for normal velocity
% Tmax      = compute maximum growth in time interval [0 Tmax]

clear

global D0 D1 D2 D4
global qb

zi = sqrt(-1);

%...input data
iflow  = input('Poiseuille (1) or Couette flow (2) ');
N      = input('Enter the number of Chebyshev polynomials: ');
Re     = input('Enter the Reynolds number: ');
alpha  = input('Enter alpha: ');
beta   = input('Enter beta: ');
Tmax   = input('Enter Tmax: ');

T      = [0 Tmax];
%...generate Chebyshev differentiation matrices
[D0,D1,D2,D4] = ChebMat(N);

%...set up Orr-Sommerfeld matrices A and B
if (iflow == 1)
  [A,B] = PoiseuilleMatrix(N,alpha,beta,Re);
else
  [A,B] = CouetteMatrix(N,alpha,beta,Re);
end

%...generate energy weight matrix
k2 = alpha^2 + beta^2;
M  = EnergyMatrix(N+1,N+1,k2);

%...compute the Orr-Sommerfeld matrix (by inverting B)
OS = inv(B)*A;
% compute and plot the eigenvalue spectrum
eee = eig(OS);
figure(3);
plot(real(eee),imag(eee),'o')
axis([-.1 alpha*1.1 -1 .1]);
grid on

%...compute the optimal
[flowin,flowot,gg] = Optimal(OS,T,M,k2,1);

%...graphics
figure(1)
semilogy(gg(:,1),gg(:,2));
grid on
figure(2)
plot(gg(:,1),gg(:,2));
grid on

output = {};

end

function output = OptimalDisturbance()

% compute the Orr-Sommerfeld matrix for three-
% dimensional Poiseuille or Couette flows and
% compute the energy weight matrix
%
% compute and displays the optimal initial condition and
% the corresponding flow response: inpout/output
%
% INPUT
%
% Re        = Reynolds number
% alpha     = alpha (streamwise wave number)
% beta      = beta  (spanwise wave number)
% iflow     = type of flow  (Poiseuille=1, Couette=2)
% N         = total number of modes for normal velocity
% T         = time of optimal growth

clear

global D0 D1 D2 D4
global qb

zi = sqrt(-1);
%...input data
iflow  = input('Poiseuille (1) or Couette flow (2) ');
N      = input('Enter the number of Chebyshev polynomials: ');
Re     = input('Enter the Reynolds number: ');
alpha  = input('Enter alpha: ');
beta   = input('Enter beta: ');
T      = input('Enter T: ');

%...generate Chebyshev differentiation matrices
[D0,D1,D2,D4] = ChebMat(N);

%...set up Orr-Sommerfeld matrices A and B
if (iflow == 1)
  [A,B] = PoiseuilleMatrix(N,alpha,beta,Re);
else
  [A,B] = CouetteMatrix(N,alpha,beta,Re);
end

%...generate energy weight matrix
k2 = alpha^2 + beta^2;
M  = EnergyMatrix(N+1,N+1,k2);

%...compute the Orr-Sommerfeld matrix (by inverting B)
OS = inv(B)*A;

%...determine optimal initial condition and optimal output
[flowin,flowot,gg] = Optimal(OS,T,M,k2,2);

%...visualize the optimal perturbation
vin    = D0*flowin(1:N+1);
etain  = D0*flowin(N+2:2*(N+1));
vout   = D0*flowot(1:N+1);
etaout = D0*flowot(N+2:2*(N+1));
ycoord = D0(:,2);

figure(1)
plot(real(vin),ycoord,'b',imag(vin),ycoord,'r', ...
  abs(vin),ycoord,'k');
title('optimal initial condition (v)')
figure(2)
plot(real(etain),ycoord,'b',imag(etain),ycoord,'r', ...
  abs(etain),ycoord,'k');
title('optimal initial condition (eta)')
figure(3)
plot(real(vout),ycoord,'b',imag(vout),ycoord,'r', ...
  abs(vout),ycoord,'k');
title('optimal output (v)')
figure(4)
plot(real(etaout),ycoord,'b',imag(etaout),ycoord,'r', ...
  abs(etaout),ycoord,'k');
title('optimal output (eta)')

output = {};

end

function output = Neutral_a_Re()

% compute the Orr-Sommerfeld matrix for three-
% dimensional Poiseuille,
% compute the energy weight matrix and determines
% maximum transient growth in the
% alpha-Re-plane
% Plot the maximum optimal growth (fig 1)
% and the least stable eigenvalue (fig 2)
% in the Reynolds-alpha plane
%
%
% INPUT
%
% beta      = beta  (spanwise wave number)
% N         = total number of modes for normal velocity
% T         = compute maximum growth in time interval [0 T]

clear

global D0 D1 D2 D4
global qb

zi = sqrt(-1);

%...input data
N      = input('Enter the number of Chebyshev polynomials: ');
beta   = input('Enter beta: ');
Tmax   = input('Enter Tmax: ');
T      = [0 Tmax];

%...generate Chebyshev differentiation matrices
[D0,D1,D2,D4] = ChebMat(N);

nreso       = 12;
Re_min      = 3000; Re_max    = 10000;
alpha_min   = 0.1;  alpha_max = 2;
Re_range    = linspace(Re_min,Re_max,nreso);
alpha_range = linspace(alpha_min,alpha_max,nreso);

for i=1:nreso
  for j=1:nreso
    alpha = alpha_range(i);
    Re    = Re_range(j);
    [A,B] = PoiseuilleMatrix(N,alpha,beta,Re);
    
    %...generate energy weight matrix
    k2 = alpha^2 + beta^2;
    M  = EnergyMatrix(N+1,N+1,k2);
    
    %...compute the Orr-Sommerfeld matrix (by inverting B)
    OS = inv(B)*A;
    eOS = eig(OS);
    
    %...compute the optimal
    [flowin,flowot,gg] = Optimal(OS,T,M,k2,1);
    Gmax(i,j) = max(gg(:,2));
    emax(i,j) = max(imag(eOS));
  end
end

%...graphics
figure(1)
contour(Re_range,alpha_range,Gmax,30);colorbar;
contour(Re_range,alpha_range,log10(Gmax),30);colorbar;
figure(2)
contour(Re_range,alpha_range,emax,30);colorbar
hold on
contour(Re_range,alpha_range,emax,[0 0],'k','LineWidth',3)

output = {};

end

function output = Neutral_alpha_beta()

% compute the Orr-Sommerfeld matrix for three-
% dimensional Poiseuille,
% compute the energy weight matrix and determines
% maximum transient growth in the
% alpha-beta-plane
% Plot the maximum optimal growth (fig 1)
% and the least stable eigenvalue (fig 2)
% in the alpha-beta plane
%
% INPUT
%
% Re        = Reynolds number
% N         = total number of modes for normal velocity
% T         = compute maximum growth in time interval [0 T]


clear

global D0 D1 D2 D4
global qb

zi = sqrt(-1);

%...input data
N      = input('Enter the number of Chebyshev polynomials: ');
Re     = input('Enter Reynolds number: ');
Tmax   = input('Enter Tmax: ');
T      = [0 Tmax];

%...generate Chebyshev differentiation matrices
[D0,D1,D2,D4] = ChebMat(N);

nreso       = 10;
alpha_min   = 0;   alpha_max = 2;
beta_min    = 0.1; beta_max  = 4;
beta_range  = linspace(beta_min,beta_max,nreso);
alpha_range = linspace(alpha_min,alpha_max,nreso);

for i=1:nreso
  for j=1:nreso
    beta  = beta_range(i);
    alpha = alpha_range(j);
    [A,B] = PoiseuilleMatrix(N,alpha,beta,Re);
    
    %...generate energy weight matrix
    k2 = alpha^2 + beta^2;
    M  = EnergyMatrix(N+1,N+1,k2);
    
    %...compute the Orr-Sommerfeld matrix (by inverting B)
    OS = inv(B)*A;
    eOS = eig(OS);
    
    %...compute the optimal
    [flowin,flowot,gg] = Optimal(OS,T,M,k2,1);
    Gmax(i,j) = max(gg(:,2));
    emax(i,j) = max(imag(eOS));
  end
end

%...graphics
figure(1)
title('Optimal growth')
contour(alpha_range,beta_range,log10(Gmax),30);colorbar
figure(2)
title('Neutral curve')
contour(alpha_range,beta_range,emax,30);colorbar
hold on
contour(alpha_range,beta_range,emax,[0 0],'k','Linewidth',3)

output = {};

end

function output = Resolvent()

%  compute the resolvent norm for real and complex frequency omega
%
% INPUT
%
% Re        = Reynolds number
% alp       = alpha (streamwise wave number)
% beta      = beta  (spanwise wave number)
% iflow     = type of flow  (Poiseuille=1, Couette=2)
% N         = total number of modes for normal velocity

clear

global D0 D1 D2 D4

zi=sqrt(-1);
% input data

iflow  = input('Poiseuille (1) or Couette flow (2) ');
N      = input('Enter the number of Chebyshev polynomials: ');
Re     = input('Enter the Reynolds number: ');
alpha  = input('Enter alpha: ');
beta   = input('Enter beta: ');

% generate Chebyshev differentiation matrices
[D0,D1,D2,D4] = ChebMat(N);

% set up Orr-Sommerfeld matrices A and B

if (iflow == 1)
  [A,B] = PoiseuilleMatrix(N,alpha,beta,Re);
else
  [A,B] = CouetteMatrix(N,alpha,beta,Re);
end

% generate energy weight matrix
k2 = alpha^2 + beta^2;
M  = EnergyMatrix(N+1,N+1,k2);

% compute the Orr-Sommerfeld matrix (by inverting B)
OS = inv(B)*A;

% compute the optimal
eee = eig(OS);
%figure(1);
%plot(real(eee),imag(eee),'o')
%axis([-2 2 -2 2]);
%axis image;

[F,e,invF] = GetMatrixParts(OS,M,k2);
nreso = 50;
for i=1:nreso
  for j=1:nreso
    zr = -0.5 + 2*(i-1)/(nreso-1);
    zi = -1 + 1.5*(j-1)/(nreso-1);
    zz = zr + sqrt(-1)*zi;
    dd = diag(1./(e-zz));
    Reso(i,j) = log(norm(F*dd*invF));
  end
end
for i=1:nreso
  zr = -0.5 + 2*(i-1)/(nreso-1);
  zz = zr;
  dd = diag(1./(e-zz));
  Reso_r(i) = (norm(F*dd*invF));
end

figure(1);subplot(1,1,1,'Fontsize',14)
semilogy(linspace(-0.5,1.5,nreso),Reso_r)
title('Resolvent norm')
ylabel('R');xlabel('\omega')
grid on
figure(2);subplot(1,1,1,'Fontsize',14)
contour(linspace(-0.5,1.5,nreso),linspace(-1,0.5,nreso),Reso')
hold on;
plot([-0.5 1.5],[0 0],'r','LineWidth',2)
plot(real(e),imag(e),'k.','MarkerSize',18);
title('Resolvent norm')
hold off

output = {};

end

function output = NumRange()

%  compute the spectrum and numerical range
%
% INPUT
%
% Re        = Reynolds number
% alpha     = alpha (streamwise wave number)
% beta      = beta  (spanwise wave number)
% iflow     = type of flow  (Poiseuille=1, Couette=2)
% N         = total number of modes for normal velocity

clear

global D0 D1 D2 D4

zi=sqrt(-1);
%...input data
iflow  = input('Poiseuille (1) or Couette flow (2) ');
N      = input('Enter the number of Chebyshev polynomials: ');
Re     = input('Enter the Reynolds number: ');
alpha  = input('Enter alpha: ');
beta   = input('Enter beta: ');

%...generate Chebyshev differentiation matrices
[D0,D1,D2,D4] = ChebMat(N);

%...set up Orr-Sommerfeld matrices A and B
if (iflow == 1)
  [A,B] = PoiseuilleMatrix(N,alpha,beta,Re);
else
  [A,B] = CouetteMatrix(N,alpha,beta,Re);
end

%...generate energy weight matrix
k2 = alpha^2 + beta^2;
M  = EnergyMatrix(N+1,N+1,k2);

%...compute the Orr-Sommerfeld matrix (by inverting B)
OS = inv(B)*A;

%...compute the numerical range
EM    = GetMatrix(OS,M,k2);
e     = eig(EM);
theta = linspace(0,2*pi,100);
for i=1:100
  th          = theta(i);
  M           = exp(sqrt(-1)*th)*EM;
  Mhat        = (M + M')/2;
  [X,D]       = eig(Mhat);
  dd          = diag(D);
  [dmax,imax] = max(dd);
  xmax        = X(:,imax);
  numran(i)   = (xmax'*EM*xmax)/(xmax'*xmax);
end

%...graphics
figure(1)
fill(real(numran),imag(numran),[0.7 0.7 0.7])
hold on;
plot(real(numran),imag(numran),'b')
plot([-0.5 1.5],[0 0],'r','LineWidth',2)
plot(real(e),imag(e),'k.','MarkerSize',18);
hold off

output = {};

end

function [D0,D1,D2,D4] = ChebMat(N)

%  Chebyshev differentiation matrices
%  (based on a hybrid pseudospectral method:
%   these matrices act on the coefficients
%   of a Chebyshev expansion, rather than
%   on the function at the collocation
%   points)
%
%  input:    N  = resolution (# of coeff)
%  output:   D0 = 0th deriv. matrix
%            D1 = 1st deriv. matrix
%            D2 = 2nd deriv. matrix
%            D4 = 4th deriv. matrix

%...create D0 (zeroth derivative)
D0  = [];
vec = (0:1:N)';
for j = 0:N
  D0 = [D0 cos(j*pi*vec/N)];
end

%...create higher derivative matrices (using the
%   Chebyshev recursion relation)
lv = length(vec);
D1 = [zeros(lv,1) D0(:,1) 4*D0(:,2)];
D2 = [zeros(lv,1) zeros(lv,1) 4*D0(:,1)];
D3 = [zeros(lv,1) zeros(lv,1) zeros(lv,1)];
D4 = [zeros(lv,1) zeros(lv,1) zeros(lv,1)];
for j = 3:N
  D1 = [D1 2*j*D0(:,j)+j*D1(:,j-1)/(j-2)];
  D2 = [D2 2*j*D1(:,j)+j*D2(:,j-1)/(j-2)];
  D3 = [D3 2*j*D2(:,j)+j*D3(:,j-1)/(j-2)];
  D4 = [D4 2*j*D3(:,j)+j*D4(:,j-1)/(j-2)];
end

end

function [A,B] = CouetteMatrix(N,alpha,beta,Re)

% Function to create Orr-Sommerfeld matrices using Chebyshev
% pseudospectral discretization for plane Couette flow
% profile
%
% N   = number of even or odd modes
% alpha     = alpha
% Re       = Reynolds number

global D0 D1 D2 D4

zi=sqrt(-1);

% mean velocity

k2  = alpha^2 + beta^2;
Nos = N+1;
Nsq = N+1;
vec = [0:N]';
u   = cos(pi*vec/N);
du  = ones(length(u),1);

% set up Orr-Sommerfeld matrix
B11 = D2 - k2*D0;
A11 = -(D4 - 2*k2*D2 + (k2*k2)*D0)/(zi*Re);
A11 = A11 + alpha*(u*ones(1,length(u))).*B11;
er  = -200*zi;
A11 = [er*[D0(1,:); D1(1,:)]; A11(3:Nos-2,:); ...
  er*[D1(Nos,:); D0(Nos,:)]];
B11 = [D0(1,:); D1(1,:); B11(3:Nos-2,:); ...
  D1(Nos,:); D0(Nos,:)];

% set up Squire matrix and (cross-term) coupling matrix
A21 = beta*(du*ones(1,length(u))).*D0;
A22 = alpha*(u*ones(1,length(u))).*D0-(D2-k2*D0)/(zi*Re);
B22 = D0;
A22 = [er*D0(1,:); A22(2:Nsq-1,:); er*D0(Nsq,:)];

% combine all the blocks
A = [A11 zeros(Nos,Nsq); A21 A22];
B = [B11 zeros(Nos,Nsq); zeros(Nsq,Nos) B22];

end

function dcoef = DerivCoeff(N)

% Compute matrix which converts Chebyshev coefficients of a
% polynomial to coefficients of derivative
%
% Reference:
% Gottlieb and Orszag, Numerical Analysis of Spectral
% Methods: Theory and Applications, SIAM, Philadelphia,
% 1977.
%
% d1  = derivative matrix (N,N)
% N   = number of coefficients

dcoef  = zeros(N,N);

for i = 0:N-1
  for j = (i+1):2:(N-1)
    dcoef(i+1,j+1) = 2*j;
  end
end
dcoef(1,:) = dcoef(1,:)/2;

end

function M = EnergyMatrix(Nos,Nsq,k2)

% Program to compute the energy weight matrix for three-
% dimensional Poiseuille and Couette flows
%
% INPUT
% Nos   = Number of normal velocity modes
% Nsq   = Number of normal vorticity modes
% k2    = alpha^2 + beta^2
%
% OUTPUT
% M     = weight matrix

M   = eye(Nos+Nsq,Nos+Nsq);
Cos = TwoNormWeight(Nos);
Dos = DerivCoeff(Nos);
Wos = Dos'*Cos*Dos + k2*Cos;
Wsq = TwoNormWeight(Nsq);

%...Orr-Sommerfeld part
[u,s,v] = svd(Wos);
s       = sqrt(diag(s));
Mos     = diag(s)*u';

%...Squire part
[u,s,v] = svd(Wsq);
s       = sqrt(diag(s));
Msq     = diag(s)*u';

%...patch together energy matrix
M       =[Mos zeros(Nos,Nsq); ...
  zeros(Nsq,Nos) Msq];

end

function x = ENormalize(x,M)

% This function normalizes the columns of x such that
%
%  || M x_i ||_2 = 1

nc = size(x,2);
for i = 1:nc
  x(:,i) = x(:,i)/norm(M*x(:,i));
end

end

function [n1,n2] = ExtractEig(e,a)

% This function computes the number of eigenvalues
% satisfying
%
% a <= Imag(lambda) <= .5
%
% INPUT
% e   = eigenvalues ordered with decreasing imaginary
%       part
%
% OUTPUT
% n1  = position of first eigenvalue in the interval
% n2  = position of last eigenvalue in the interval

n1 = 1;
while (imag(e(n1)) > 0.5)
  n1 = n1+1;
end

n2 = n1;
while (imag(e(n2)) > a)
  n2 = n2+1;
end
n2 = n2-1;

end

function EM = GetMatrix(OS,M,k2)

% This function computes the energy weighted
% OS equation
%
% INPUT
% OS       = 3D Orr-Sommerfeld operator
% M        = energy weight matrix
% k2       = alpha^2+beta^2
%
% OUTPUT
% EM       = energy-weighted OS matrix

% Phase 1: Compute eigenvalues and eigenfunctions of
% Orr-Sommerfeld matrix and sort in order of descending
% imaginary part. The function nlize normalizes the
% eigenfunctions with respect to the weight matrix M.

[xs,es] = OrderedEig(OS);
xs      = ENormalize(xs,M);

% Phase 2: Choose which modes are to be used in optimal
% calculation. Modes with imaginary part > 1 are neglected.
% Modes with imaginary part < imin are neglected as well.

ishift = 1;
imin   = -1.5;

while imag(es(ishift))>1,
  ishift = ishift+1;
end

[n1,n2] = ExtractEig(es,imin);

cols    = (ishift:n2);
xu      = xs(:,cols);
eu      = es(cols);
ncols   = length(cols);
fprintf('Number of modes used: %1.0f \n',ncols);

% Phase 3: Compute the reduced Orr-Sommerfeld operator

[qb,invF] = TMatrix(M,xu,eu);
EM        = sqrt(-1)*qb;

end

function [F,e,invF] = GetMatrixParts(OS,M,k2)

% This function computes the energy weighted
% OS equation, as a siagonal matrix with eigenvalues
% and F such that the energy norm is the 2-norm of Fu
%
% INPUT
% OS       = 3D Orr-Sommerfeld operator
% M        = energy weight matrix
% k2       = alpha^2+beta^2
%
% OUTPUT
% OUTPUT
% e     =  matrix of eigenvalues
% F     =  matrix defining the energy norm in the reduced space
%          based on eigenvectors E_norm(u)=2_Norm(Fu)
% invF  =  inverse of F

% Phase 1: Compute eigenvalues and eigenfunctions of
% Orr-Sommerfeld matrix and sort in order of descending
% imaginary part. The function nlize normalizes the
% eigenfunctions with respect to the weight matrix M.

[xs,es] = OrderedEig(OS);
xs      = ENormalize(xs,M);

% Phase 2: Choose which modes are to be used in optimal
% calculation. Modes with imaginary part > 1 are neglected.
% Modes with imaginary part < imin are neglected as well.

ishift = 1;
imin   = -1.5;

while imag(es(ishift))>1,
  ishift = ishift+1;
end

[n1,n2] = ExtractEig(es,imin);

cols    = (ishift:n2);
xu      = xs(:,cols);
eu      = es(cols);
ncols   = length(cols);
fprintf('Number of modes used: %1.0f \n',ncols);

% Phase 3: Compute the reduced Orr-Sommerfeld operator

[F,e,invF] = TMatrixParts(M,xu,eu);

end


function a = NormMExp(t)

% This function computes the norm of the matrix exponential of qb*t

global qb

a = -norm(expm(t*qb));

end


function t=OptFunc(t1,t2,options)

% This function uses the built-in function 'fminbnd' to find
% the maximum value of a function on the interval [t1,t2].
% The function is in the file NormMExp.m
%
% INPUT:
% t1,t2   = lower and upper bounds of interval
% options = input parameters for minimization routine
%
% OUTPUT
% t       = value at which function NormMExp(t) is minimized

f1     = NormMExp(t1);
f2     = NormMExp(t2);
tt     = fminbnd(@NormMExp,t1,t2,options);

f3     = NormMExp(tt);
f      = [f1 f2 f3];
tm     = [t1 t2 tt];
[y,is] = sort(f);
t      = tm(is(1));

end

function [flowin,flowot,gg] = Optimal(OS,T,M,k2,iflag)

% This function computes the initial flow structure which
% achieves the maximum transient energy growth
%
% INPUT
% OS      = 3D Orr-Sommerfeld operator
% T       = time
% M       = energy weight matrix
% k2     = alpha^2+beta^2
% iflag   = flag
%           iflag = 1:  compute the maximum growth and
%                       initial condition in time
%                       interval [0,T]
%           iflag = 2:  compute the initial disturbance
%                       yielding maximum growth at time T
% OUTPUT
% flowin  = coefficients of optimal disturbance
%           flowin(1:Nos)         = normal velocity
%                                   coefficients
%           flowin(Nos+1:Nos+Nsq) = normal vorticity
%                                   coefficients
% flowot  = coefficients of field at optimal time
%           flowot(1:Nos)         = normal velocity
%                                   coefficients
%           flowot(Nos+1:Nos+Nsq) = normal vorticity
%                                   coefficients
% gg      = energy growth curve (t,G(t))

global qb

% Phase 1: Compute eigenvalues and eigenfunctions of
% Orr-Sommerfeld matrix and sort in order of descending
% imaginary part. The function nlize normalizes the
% eigenfunctions with respect to the weight matrix M.

[xs,es] = OrderedEig(OS);
xs      = ENormalize(xs,M);

% Phase 2: Choose which modes are to be used in optimal
% calculation. Modes with imaginary part > 1 are neglected.
% Modes with imaginary part < imin are neglected as well.

ishift = 1;
imin   = -1.5;

while imag(es(ishift))>1,
  ishift = ishift+1;
end

[n1,n2] = ExtractEig(es,imin);

cols    = (ishift:n2);
xu      = xs(:,cols);
eu      = es(cols);
ncols   = length(cols);
fprintf('Number of modes used: %1.0f \n',ncols);

% Phase 3: Compute the reduced Orr-Sommerfeld operator

[qb,invF] = TMatrix(M,xu,eu);

% Phase 4: Compute the time for the maximum growth using
% the built-in Matlab routine 'fminbnd'

if (iflag == 1)
  gcheck = NormMExp(1/100);
  gcheck = gcheck^2;
  if (gcheck < 1)
    tformax = 0;
    mgrowth = 1;
  else
    ts = T(1);
    tf = T(2);
%     options = [0 1e-3 1e-3];
    options = optimset('TolX', 1e-3);
    tformax = OptFunc(ts,tf,options);
    mgrowth = NormMExp(tformax);
    mgrowth = mgrowth^2;
  end
  fprintf('Time for maximum growth:  %e \n',tformax);
else
  tformax = T;
end

% Phase 5: Compute the initial condition that yields the
% maximum growth. This is obtained by
% (1) computing the matrix exponential evaluated at the
%     optimal time;
% (2) computing the SVD of the matrix exponential
%     exp(-i*A*t)=USV.
% The initial condition that yields the maximum growth is
% the first column of V. To convert the initial condition
% to a vector of coefficients in the eigenfunction basis
% multiply by the matrix of eigenfunctions and inv(F)

evol    = expm(tformax*qb);
[U,S,V] = svd(evol);
mgrowth = S(1,1)^2;
fprintf('Maximum growth in energy:  %e \n',mgrowth);

flowin = sqrt(2*k2)*xu*invF*V(:,1);
flowot = sqrt(2*k2)*xu*invF*U(:,1);

if (iflag==1)
  ts = 0;
  tf = T(2);
else
  ts=0;
  tf=T(1);
end


nit=100;
for i = 1:nit
  tid     = ts + (tf-ts)/(nit-1)*(i-1);
  gg(i,1) = tid;
  gg(i,2) = norm(expm(tid*qb))^2;
end

%        nit=100;
%      gg(:,1) = logspace(-3,(log10(tf)),nit);
%     for i = 1:nit
%       gg(i,2) = norm(expm(gg(i,1)*qb))^2;
%     end

end

function [xs,es] = OrderedEig(A)

% This function computes the eigenvalues of a matrix A and
% orders the eigenvalues so that the imaginary parts are
% decreasing.
%
% INPUT
% A    = input matrix
%
% OUTPUT
% es   = ordered eigenvalues
% xs   = eigenvectors

[v,e]      = eig(A);
e          = diag(e);
[eimag,is] = sort(-imag(e));
xs         = v(:,is);
es         = e(is);

end

function [A,B] = PoiseuilleMatrix(N,alpha,beta,Re)

% create Orr-Sommerfeld matrices using Chebyshev
% pseudospectral discretization for plane Poiseuille flow
% profile
%
% N  = number of modes
% alpha     = alpha
% beta    = beta
% Re       = Reynolds number

global D0 D1 D2 D4

zi=sqrt(-1);

% mean velocity

k2 = alpha^2 + beta^2;
Nos = N+1;
Nsq = N+1;
vec = [0:N]';
u   = (ones(length(vec),1)-cos(pi*vec/N).^2);
du  = -2*cos(pi*vec/N);

% set up Orr-Sommerfeld matrix
B11 = D2 - k2*D0;
A11 = -(D4 -2*k2*D2 + (k2*k2)*D0)/(zi*Re);
A11 = A11 + alpha*(u*ones(1,length(u))).*B11 + alpha*2*D0;
er  = -200*zi;
A11 = [er*D0(1,:); er*D1(1,:); A11(3:Nos-2,:); ...
  er*D1(Nos,:); er*D0(Nos,:) ];
B11 = [D0(1,:); D1(1,:); B11(3:Nos-2,:); ...
  D1(Nos,:); D0(Nos,:)];

% set up Squire matrix and (cross-term) coupling matrix
A21 = beta*(du*ones(1,length(u))).*D0(1:Nos,:);
A22 = alpha*(u*ones(1,length(u))).*D0-(D2-k2*D0)/(zi*Re);
B22 = D0;
A22 = [er*D0(1,:); A22(2:Nsq-1,:); er*D0(Nsq,:)];
A21 = [zeros(1,Nos); A21(2:Nsq-1,:); zeros(1,Nos)];

% combine all the blocks
A = [A11 zeros(Nos,Nsq); A21 A22];
B = [B11 zeros(Nos,Nsq); zeros(Nsq,Nos) B22];

end

function [qb,invF] = TMatrix(M,xu,e)

% This function computes the matrix  Q=-i*F*diag(e)*inv(F)
% which is used to compute the maximum transient growth
% (see Reddy and Henningson, "Energy Growth in Viscous
% Channel Flows", JFM 252, page 209, 1993).
%
% INPUT
% M     =  energy weight matrix
% xu    =  matrix of eigenfunctions (expansion coefficients)
% e     =  vector of eigenvalues of the stability matrix
%
% OUTPUT
% qb    =  output matrix Q
% invF  =  inverse of F

% Phase 1: compute inner product of the eigenfunctions
%          in energy norm
work = M*xu;
A    = work'*work;

% Phase 2: compute decomposition A=F^*F
[U,S,V] = svd(A);
s       = sqrt(diag(S));
F       = diag(s)*U';
invF    = U*diag(ones(size(s))./s);

% Phase 3: compute Q=-i*F*diag(e)*inv(F)
qb      = -sqrt(-1)*F*diag(e)*invF;

end

function [F,e,invF] = TMatrixParts(M,xu,e)

% This function computes the matrix  Q=-i*F*diag(e)*inv(F)
% which is used to compute the maximum transient growth
% (see Reddy and Henningson, "Energy Growth in Viscous
% Channel Flows", JFM 252, page 209, 1993).
%
% INPUT
% M     =  energy weight matrix
% xu    =  matrix of eigenfunctions (expansion coefficients)
% e     =  vector of eigenvalues of the stability matrix
%
% OUTPUT
% e     =  matrix of eigenvalues
% F     =  matrix defining the energy norm in the reduced space
%          based on eigenvectors E_norm(u)=2_Norm(Fu)
% invF  =  inverse of F

% Phase 1: compute inner product of the eigenfunctions
%          in energy norm
work = M*xu;
A    = work'*work;

% Phase 2: compute decomposition A=F^*F
[U,S,V] = svd(A);
s       = sqrt(diag(S));
F       = diag(s)*U';
invF    = U*diag(ones(size(s))./s);

end

function c = TwoNormWeight(N)

% This program determines the two norm weight matrix c for
% Chebyshev polynomials. The matrix c is defined by
%
% c_{ij}= int_{-1}^{1} T_{i}(x) T_{j}(x) dx,
%
% where T_k is a Chebyshev polynomial. The above product satisfies
%
% c_{ij} = 1/(1-(i+j)^2)+1/(1-(i-j)^2)   for i+j even
%        = 0                             for i+j odd
%
% Input
%
% N    = number of modes
%
% The maximum degree M of polynomials c_i,c_j satisfies M = N-1

c   = zeros(N,N);

for i = 0:N-1
  for j = 0:N-1
    if (rem(i+j,2) == 0)
      p          = 1/(1-(i+j)^2)+1/(1-(i-j)^2);
      c(i+1,j+1) = p;
    else
      c(i+1,j+1) = 0;
    end
  end
end

end

