function [] = plotBiGEigenvalues()
% plots eigenvalues from run_BiG()
    
    % clearscreen
    clc;
    
    % plot all parallel examples or only single core example
    parallel = 0;
    
    if parallel == 1
        
        % data files
        data_files = ["np1/EigenValue.txt" , "np2/Eigenvalue.txt", ...
                      "np4/EigenValue.txt" , "np8/EigenValue.txt", ...
                      "np16/EigenValue.txt", "np32/EigenValue.txt"];

        figure;
        hold on;
        for i = 1 : length(data_files)
            % import data
            data = importdata(data_files(i));
    
            % plot 
            plot(data(:,1), data(:,2), '+');
        end
        hold off;
        grid on;
        legend(data_files);
        
    else
        
        data = importdata("EigenValue.txt");
        
        figure;
        hold on;
        plot(data(:,1), data(:,2), 'x');
        run('Main_1_ResolventAnalysis.m');
        
        select_script_fixed('TransientGrowth');
        xlim([ 0.0, 1.0]);
        ylim([-1.0, 0.0]);
        hold off;
        
    end
    
end

