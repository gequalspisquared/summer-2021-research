/*
Testing out cuda for possible implementation 
into CharLin later on

Written by: Nicholas Crane
*/

#include <stdio.h>
#include <cuda.h>

__global__ void cuda_hello()
{
    printf("Hello World from GPU!\n");
}

int main()
{
    cuda_hello<<<1,1>>>();
    return 0;
}