#include "LinUgpCvCfIdealGas.hpp"

// ========================================================
//
// Compressible LES solver: charles
//
// Solves the compressible Navier-Stokes equations based
// on constant-gamma ideal gas law.
//
// ========================================================

// an example of how to customize charles. Note that you
// also have to instantiate this executable in the main at the
// bottom of this file.

class MyCharlin : public LinUgpCvCfIdealGas
{

private:
    double gamma, rho_ref, p_ref, T_ref;

public:
    MyCharlin()
    {

        if (mpi_rank == 0)
            cout << "MyCharlin()" << endl;

        // get input parameters...
        gamma = getDoubleParam("GAMMA", 1.4);
        rho_ref = getDoubleParam("RHO_REF");
        p_ref = getDoubleParam("P_REF");
        T_ref = getDoubleParam("T_REF");
    }

    // set an initial condition...
    void initialHook()
    {

        // the initial condition should only be applied when the
        // step is zero...

        if (mpi_rank == 0) {
            cout << "MyCharlin::initialHook()" << endl;
            cout << "{{{ RUNNING INITIAL HOOK }}}\n";
            cout << "{{{ Step = " << step << " }}}\n";
        }

        //if (step == 0)
        //{
            //cout << "{{{ STEP == 0! }}}\n";
            // for an initial condition, set the conserved variables...
            double (*u_avg)[3] = getR2("U_AVG");
            double *p_avg      = getR1("P_AVG");
            double *T_avg      = getR1("T_AVG");
            double *rho_avg    = getR1("RHO_AVG");

            cout << "{{{ ncv = " << ncv << " }}}\n";

            // For linear solver, specify base state and perturbations in step 1 and 2...
            FOR_ICV
            {

                double x = x_cv[icv][0];
                double y = x_cv[icv][1];

                // Step 1. Initialize Base States
		        // /* FIX THIS, SPECIFY THE AVG VALUES!!! */

                // rho_bar[icv] = rho_ref;
                // p_bar[icv] = p_ref;
                // T_bar[icv] = T_ref;
                // u_bar[icv][0] = //0.0; //0.1*(1.0-pow(y,2.0));
                // u_bar[icv][1] = //0.0; //0.1*(1.0-pow(x,2.0));
                //u_bar[icv][2] = 0.004331 * (1.0 - pow(x, 2.0)); //0.0025 * x;
                u_bar[icv][0] = u_avg[icv][0];
                u_bar[icv][1] = u_avg[icv][1];
                u_bar[icv][2] = 0.0;
                p_bar[icv]    = p_avg[icv];
                T_bar[icv]    = T_avg[icv];
                rho_bar[icv]  = rho_avg[icv];

                FOR_I3 rhou_bar[icv][i] = rho_bar[icv] * u_bar[icv][i];
                rhoE_bar[icv] = p_bar[icv] / (gamma - 1.0) + 0.5 * rho_bar[icv] * DOT_PRODUCT(u_bar[icv], u_bar[icv]);
            }
        //}
    }

    // Apply sponge region to boudary while building linear operater L
    /*
    void calcSourceHook(complex<double> *out)
    {

        // parse sponge parameters...

        int idir = 0;
        double sponge_length = 0.1;
        double sponge_strength = 0.5;
        const double Lmax = 1;
        double sponge_coeff;

        FOR_ICV
        {
            if (x_cv[icv][idir] > Lmax - sponge_length)
            {
                const double x_sp = (x_cv[icv][idir] - (Lmax - sponge_length)) / sponge_length;
                // optimized sponge profile: a*x^2 + b*x^8
                sponge_coeff = sponge_strength * (0.068 * pow(x_sp, 2.0) + 0.845 * pow(x_sp, 8.0));

                out[Apntr[icv][0]] -= cv_volume[icv] * sponge_coeff * (dcomp(rhor[icv], rhoi[icv]) - 0.0);
                FOR_I3 out[Apntr[icv][i + 1]] -= cv_volume[icv] * sponge_coeff * (dcomp(rhour[icv][i], rhoui[icv][i]) - 0.0);
                out[Apntr[icv][4]] -= cv_volume[icv] * sponge_coeff * (dcomp(rhoEr[icv], rhoEi[icv]) - 0.0);
            }
        }
    }
    */

    // do something after every time step...
    void temporalHook()
    {

        if (step % check_interval == 0)
        {

            if (mpi_rank == 0)
                cout << "MyCharles::temporalHook()" << endl;
        }
    }
};

// ======================================================
// main
// ======================================================

int main(int argc, char *argv[])
{

    try
    {

        // initialize the environment: basically
        // initializes MPI and parameters...

        CTI_Init(argc, argv, "charlin.in");

        // the solver...

        {

            MyCharlin solver;
            solver.init_BiG();
            //solver.run_Resolvent();
            solver.run_BiG();
	    solver.run_Resolvent();
        }

        // finalize the environment: reports parameter usage, shuts down MPI...

        CTI_Finalize();

        // error management...
    }
    catch (int e)
    {
        if (e == 0)
            CTI_Finalize();
        else
            CTI_Abort();
    }
    catch (...)
    {
        CTI_Abort();
    }

    return (0);
}
