function [] = magma_zgeev_performance()

    % Performance analysis of zgeev
    %
    % Written by Nicholas Crane
    
    % clearscreen
    clc;
    
    % data from test
    N = [1088, 2112, 3136, 4160, 5184, 6208, 7232];
    t = [3.58, 12.34, 32.57, 66.35, 110.56, 182.99, 244.80];
    
    % plot the log of the data and perform a linear fit to 
    % determine O(n^a) order (a)
    loglog(N, t);
    hold on;
    %loglog(N, t);
    fit = polyfit(log10(N), log10(t), 1);
    plot(N, fit(1)*N + fit(2));
    hold off;
    grid on;
    xlabel("N");
    ylabel("log(t)");
    title ("log(t) vs N for magma\_zgeev");
    
    % print out order of n
    fprintf("Order = %f\n", fit(1));
    
    % print time estimate to find eigenvalues for N = 100,000
    t_estimate = (1000000/N(1))^fit(1)*t(1);
    fprintf("t(N = 100,000) = %fs, %fm %fh\n", t_estimate, t_estimate/60, ...
                                        t_estimate/60/60);
    
end

